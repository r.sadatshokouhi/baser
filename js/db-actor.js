(function ($) {
  (function () {

    var db = {

      loadData: function (filter) {
        return $.grep(this.clients, function (client) {
          return (!filter.name || client.name.indexOf(filter.name) > -1) &&
            (filter.order === undefined || client.order === filter.order) &&
            (!filter.kind || client.kind === filter.kind) &&
            (filter.timing === undefined || client.timing === filter.timing);
        });
      },

      insertItem: function (insertingClient) {
        this.clients.push(insertingClient);
      },

      updateItem: function (updatingClient) {},

      deleteItem: function (deletingClient) {
        var clientIndex = $.inArray(deletingClient, this.clients);
        this.clients.splice(clientIndex, 1);
      }

    };
    // console.log("start");
    // var token = localStorage.getItem('token');
    // const evtSource = new EventSource(`http://10.100.100.15:5000/sensors?token=${token}`);
    // evtSource.onopen = function() {
    // console.log("SSE onopen");
    // }
    window.db = db;
    
    // evtSource.addEventListener("message", function(event) {
    //   db.clients = event.data;
    // });

    
    db.kinds = [
      {
        Name: "اکتور",
        Id: 4
      }
    ];

    db.clients = [
      {
          "id" : 1,
          "kind" : "اکتور",
          "type" : "default",
          "name" : "مخزن۱",
          "status" : "on",
          "order" : 2,
          "timing": true
      },
      {
          "id" : 2,
          "kind" : "اکتور",
          "type" : "default",
          "name" : "مخزن۲",
          "status" : "off",
          "order" : 1,
          "timing": true
      },
      {
          "id" : 3,
          "kind" : "اکتور",
          "type" : "default",
          "name" : "استخر",
          "status" : "on",
          "order" : 4,
          "timing": true
      },
      {
          "id" : 4,
          "kind" : "اکتور",
          "type" : "default",
          "name" : "باغچه۱",
          "status" : "off",
          "order" : 55,
          "timing": true
      },
      {
          "id" : 5,
          "kind" : "اکتور",
          "type" : "default",
          "name" : "باغچه۲",
          "status" : "off",
          "order" : 32,
          "timing": true
      }
  ];
    console.log(typeof(db.clients));

  }());
})(jQuery);