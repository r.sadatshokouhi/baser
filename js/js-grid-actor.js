
(function ($) {
  'use strict';
  $(function () {

    //basic config
    if ($("#js-grid").length) {
      $("#js-grid").jsGrid({
        height: "500px",
        width: "100%",
        filtering: true,
        editing: true,
        inserting: true,
        sorting: true,
        paging: true,
        autoload: true,
        pageSize: 15,
        pageButtonCount: 5,
        deleteConfirm: "آیا از حذف این ردیف مطمئن هستید؟",
        data: db.clients,
        fields: [{
            name: "name",
            title: "نام",
            type: "text",
            width: 150
          },
          {
            name: "order",
            title: "ترتیب",
            type: "number",
            width: 50
          },
          {
            name: "id",
            title: "شناسه",
            type: "text",
            width: 200
          },
          {
            name: "kind",
            title: "نوع",
            type: "select",
            items: db.kinds,
            valueField: "Name",
            textField: "Name"
          },
          {
            name: "timing",
            title: "زمانبندی",
            itemTemplate: function (value, item) {
              return $("<button>").attr("type", "button").addClass("btn btn-primary btn-sm").html('زمان بندی')
              .on("click", function(){
                window.location.href = "schedule.html?actorName=" + item.name;
              });
            }
          },
          {
            type: "control"
          }
        ]
      
      });
      
  
    }

    if ($("#sort").length) {
      $("#sort").on("click", function () {
        var field = $("#sortingField").val();
        $("#js-grid-sortable").jsGrid("sort", field);
      });
    }

    
  });
})(jQuery);