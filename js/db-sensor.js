(function ($) {
    (function () {
  
      var db = {
  
        loadData: function (filter) {
          return $.grep(this.clients, function (client) {
            return (!filter.name || client.name.indexOf(filter.name) > -1) &&
              (filter.order === undefined || client.order === filter.order) &&
              (!filter.kind || client.kind === filter.kind) &&
              (filter.timing === undefined || client.timing === filter.timing);
          });
        },
  
        insertItem: function (insertingClient) {
          this.clients.push(insertingClient);
        },
  
        updateItem: function (updatingClient) {},
  
        deleteItem: function (deletingClient) {
          var clientIndex = $.inArray(deletingClient, this.clients);
          this.clients.splice(clientIndex, 1);
        }
  
      };
      // console.log("start");
      // var token = localStorage.getItem('token');
      // const evtSource = new EventSource(`http://10.100.100.15:5000/sensors?token=${token}`);
      // evtSource.onopen = function() {
      // console.log("SSE onopen");
      // }
      window.db = db;
      
      // evtSource.addEventListener("message", function(event) {
      //   db.clients = event.data;
      // });
  
      
      db.kinds = [{
          Name: "",
          Id: 0
        },
        {
          Name: "دما",
          Id: 1
        },
        {
          Name: "رطوبت",
          Id: 2
        },
        {
          Name: "مخزن آب",
          Id: 3
        },
        {
          Name: "اکتور",
          Id: 4
        }
      ];
  
      db.clients = [
        {
            "id" : 1,
            "type" : "angular",
            "kind" : "رطوبت",
            "name" : "باغچه پایینی",
            "order" : 3
        },
        {
            "id" : 2,
            "type" : "cylinder",
            "kind" : "مخزن آب",
            "name" : "مخزن آب",
            "order" : 1
        },
        {
            "id" : 4,
            "type" : "angular",
            "kind" : "رطوبت",
            "name" : "کنار استخر",
            "order" : 5
        },
        {
            "id" : 3,
            "type" : "thermometer",
            "kind" : "دما",
            "name" : "دماسنج باغ",
            "order" : 2
        },
        {
            "id" : 5,
            "type" : "thermometer",
            "kind" : "دما",
            "name" : "دماسنج گلخونه",
            "order" : 7
        },
        {
            "id" : 6,
            "type" : "angular",
            "kind" : "رطوبت",
            "name" : "باغچه جلوی در",
            "order" : 10
        }
    ];
      console.log(typeof(db.clients));
  
    }());
  })(jQuery);