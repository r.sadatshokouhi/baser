(function ($) {
  (function () {

    var db = {

      // loadData: function (filter) {
      //   return $.grep(this.clients, function (client) {
      //     return (!filter.name || client.name.indexOf(filter.name) > -1) &&
      //       (filter.type === undefined || client.type === filter.type) &&
      //       (filter.timing === undefined || client.timing === filter.timing);
      //   });
      // },

      // insertItem: function (insertingClient) {
      //   this.clients.push(insertingClient);
      // },

      // updateItem: function (updatingClient) {},

      deleteItem: function (deletingClient) {
        var clientIndex = $.inArray(deletingClient, this.clients);
        this.clients.splice(clientIndex, 1);
      }

    };

    window.db = db;

    db.activation =[
      {
        Name: "",
        Id: "-1"
      },
      {
        Name: "فعال",
        Id: "1"
      },
      {
        Name: "غیرفعال",
        Id: "0"
      }
    ];

    db.frequency =[
      {
        Name: "",
        Id: "-1"
      },
      {
        Name: "روزانه",
        Id: "1"
      },
      {
        Name: "هفتگی",
        Id: "2"
      },
      {
        Name: "ماهیانه",
        Id: "3"
      }
    ];

    db.clients = [
      {
        "ID" : 1,
        "frequency" : "2",
        "name" : "مخزن۱۵",
        "active" : "1",
        "day" : "1",
        "weekCount" : "10"
        },
        {
          "ID" : 1,
          "frequency" : "2",
          "name" : "مخزن۱۴",
          "active" : "1",
          "day" : "1",
          "weekCount" : "10"
          },
          {
            "ID" : 1,
            "frequency" : "2",
            "name" : "مخزن۱۳",
            "active" : "1",
            "day" : "1",
            "weekCount" : "10"
            },
            {
              "ID" : 1,
              "frequency" : "2",
              "name" : "مخزن۱",
              "active" : "1",
              "day" : "1",
              "weekCount" : "10"
              },
      {
          "frequency" : "1",
          "kind" : "اکتور",
          "type" : "default",
          "name" : "مخزن۱",
          "status" : "on",
          "type" : 2,
          "timing": true
      },
      {
          "frequency" : "3",
          "kind" : "اکتور",
          "type" : "default",
          "name" : "استخر",
          "status" : "on",
          "type" : 4,
          "timing": true
      },
      {
          "frequency" : "1",
          "type" : "default",
          "name" : "باغچه۱",
          "status" : "off",
          "type" : 55,
          "timing": true
      }
  ];
    

  }());
})(jQuery);