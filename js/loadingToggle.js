function drawActors(staticActorsData){
    staticData = staticActorsData;
    //build html code
    var toggles = '<div class="owl-carousel owl-theme full-width" >'; //we add the carousel here so it show the slide
    for (let index = 0; index < staticData.length; index++) {
        toggles += drawLoddingTogle(staticData[index].id, staticData[index].name);
    }
    toggles += '</div>';
    document.getElementById("actor").innerHTML = toggles;
    //set status
    for (let index = 0; index < staticData.length; index++) {
        if (staticData[index].status == "on")
            $("#activate-btn-" + staticData[index].id).addClass("loading done");
    }
}

function updateActors(evtSource){
    evtSource.addEventListener("message", function(event) {
        var content = JSON.parse(event.data);
        for(var i = 0; i < content.length; i++){
            //first check whether its for actors
            if (content[index].status == "on")
                $("#activate-btn-" + content[index].id).addClass("loading done");
            if (content[index].status == "off")
                $("#activate-btn-" + content[index].id).removeClass("loading done");
            // if (content[i].rout == ){
            //     var newData = setAngularData(String(content[i].data), String(content[i].timestamp), content[i].data);
            //     chartObj.setJSONData(newData);
            // }              
        }      
    });
}

function drawLoddingTogle(id, name){
    return '<div class="item">' + 
                '<div class="activte-but" >' +
                    '<a class="activate" id="activate-btn-' + id + '">' +
                        '<span>' +
                        '<svg>' +
                            '<use xlink:href="#circle"/>' +
                        '</svg>' +
                        '<svg>' +
                            '<use xlink:href="#arrow"/>' +
                        '</svg>' +
                        '<svg>' +
                            '<use xlink:href="#check"/>' +
                        '</svg>' +
                        '</span>' +
                    '<ul>' +
                        '<li>فعال کردن</li>' +
                        '<li>انتظار</li>' +
                        '<li>فعال شد</li>' +
                    '</ul>' +
                    '</a>' +
                    '<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">' +
                        '<symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" id="circle">' +
                            '<circle cx="8" cy="8" r="7.5"></circle>' +
                        '</symbol>' +
                        '<symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12" id="arrow">' +
                            '<path d="M2.7008908,5.37931459 L2.7008908,5.37931459 C2.9224607,5.60207651 3.2826628,5.60304283 3.50542472,5.38147293 C3.52232305,5.36466502 3.53814843,5.34681177 3.55280728,5.32801875 L5.34805194,3.02646954 L5.34805194,10.3480519 C5.34805194,10.7081129 5.63993903,11 6,11 L6,11 C6.36006097,11 6.65194806,10.7081129 6.65194806,10.3480519 L6.65194806,3.02646954 L8.44719272,5.32801875 C8.6404327,5.57575732 8.99791646,5.61993715 9.24565503,5.42669716 C9.26444805,5.41203831 9.28230129,5.39621293 9.2991092,5.37931459 L9.2991092,5.37931459 C9.55605877,5.12098268 9.57132199,4.70855346 9.33416991,4.43193577 L6.75918715,1.42843795 C6.39972025,1.00915046 5.76841509,0.960656296 5.34912761,1.32012319 C5.31030645,1.35340566 5.27409532,1.38961679 5.24081285,1.42843795 L2.66583009,4.43193577 C2.42867801,4.70855346 2.44394123,5.12098268 2.7008908,5.37931459 Z"></path>' +
                        '</symbol>' +
                        '<symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12" id="check">' +
                            '<path id="test" d="M4.76499011,6.7673683 L8.2641848,3.26100386 C8.61147835,2.91299871 9.15190114,2.91299871 9.49919469,3.26100386 C9.51164115,3.27347582 9.52370806,3.28637357 9.53537662,3.29967699 C9.83511755,3.64141434 9.81891834,4.17816549 9.49919469,4.49854425 L5.18121271,8.82537365 C4.94885368,9.05820878 4.58112654,9.05820878 4.34876751,8.82537365 L2.50080531,6.97362503 C2.48835885,6.96115307 2.47629194,6.94825532 2.46462338,6.93495189 C2.16488245,6.59321455 2.18108166,6.0564634 2.50080531,5.73608464 C2.84809886,5.3880795 3.38852165,5.3880795 3.7358152,5.73608464 L4.76499011,6.7673683 Z"></path>' +
                        '</symbol>' +
                    '</svg>' +
                '</div>' +
                '<div class="mt-auto text-center w-100">' +
                    '<h5 style="text-align: center;">' +
                    ' اکتور' +  id + '</br>' +
                    '<br/>نام :  '+ name +
                    '</h5>' +
                    '<h6 class="card-text mb-4 font-weight-normal"></h6>' +
                '</div>'+
            '</div>';
}

$(".activate").on("click touch", function (e) {
    var self = $(this);
    
    if (!self.hasClass("loading")) {
        self.addClass("loading");
        setTimeout(function () {
            self.addClass("done");
        }, 3200);
    }
    else{
        self.removeClass("loading done");
        self.addClass("loading");
        setTimeout(function () {
            self.addClass("done");
            self.removeClass("loading done");
        }, 3200);
    }
});
