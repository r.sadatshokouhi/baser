
(function ($) {
  'use strict';
  $(function () {

    //basic config
    if ($("#js-grid").length) {
      $("#js-grid").jsGrid({
        height: "500px",
        width: "100%",
        filtering: true,
        editting: true,
        sorting: true,
        paging: true,
        autoload: true,
        pageSize: 15,
        pageButtonCount: 5,
        deleteConfirm: "آیا مطمئن هستید؟",
        data: db.clients,
        fields: [{
            name: "name",
            title: "نام",
            type: "text",
            width: 150
          },
          {
            name: "active",
            title: "وضعیت",
            type: "select",
            items: db.activation,
            valueField: "Id",
            textField: "Name"
          },
          {
            name: "frequency",
            title: "توضیح",
            type: "select",
            items: db.frequency,
            valueField: "Id",
            textField: "Name"
          },
          {
            name: "timing",
            title: "زمانبندی",
            itemTemplate: function (value, item) {
              return $(
              '<div class="text-center">' +
                '<button type="button" class="btn btn-primary btn-xs"' +
                  '>ویرایش <i class="fas fa-edit"></i></button>' +
              '</div>' ).
              on("click", function () {
                document.getElementById("grid-schedule").style.display = "none";
                document.getElementById("schedule").style.display = "block";
                document.getElementById("submit").classList.remove("insert");
                document.getElementById("submit").classList.add("update");
                $('form').dejsonify(item);
                
              });;
            }
          },
          {
            type: "control",
            editButton: false,
            headerTemplate: function() {
              return $("<button>").attr("type", "button").addClass("btn btn-primary btn-sm").html('جدید <i class="fas fa-plus"></i>')
                      .on("click", function () {
                        document.getElementById("grid-schedule").style.display = "none";
                        document.getElementById("schedule").style.display = "block";
                        document.getElementById("submit").classList.remove("update");
                        document.getElementById("submit").classList.add("insert");
                      });
                    }
          }
      ]
      });
    }

  document.getElementById("schedule").style.display = "none";
  
    
  });
})(jQuery);

