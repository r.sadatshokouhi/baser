
// notifications timeline part
var notifTime = new persianDate().format("dddd, DD MMMM YYYY, h:mm:ss a");
var notifInfo = {
    "notifTime" : notifTime,
    "notifContent" : 'نوتیفیکیشن ساختگی و جدید که در زمان اجرای برنامه از جیسون گرفته و نشان داده شده.'
};

function showNewNotification(notif){

    //add new notif to the first of the timeline
    var preNotifs = document.getElementById("timeline").innerHTML;

    //find out if inverting the timeline is necessary
    var count = (preNotifs.match(/timeline-wrapper/g) || []).length;
    var notifPlace = '';
    if (count % 2){
        notifPlace = '';
    }else {
        notifPlace = 'timeline-inverted ';
    }
    
    var newNotif = '<div class="timeline-wrapper ' + notifPlace + 'timeline-wrapper-warning' + '">' +
                        '<div class="timeline-badge"></div>' +
                        '<div class="timeline-panel">' +
                            '<div class="timeline-heading">' +
                                '<h6 class="timeline-title">' +
                                    'تست ایپسوم' +
                                '</h6>' +
                            '</div>' +
                            '<div class="timeline-body">' +
                                '<p>' +
                                    notif.notifContent +
                                '</p>' +
                            '</div>' +
                            '<div class="timeline-footer d-flex align-items-center">' +
                                '<span class="mr-auto font-weight-bold">' +
                                    notif.notifTime +
                                '</span>' +
                            '</div>' +
                        '</div>' +
                    '</div>';
    document.getElementById("timeline").innerHTML = newNotif + preNotifs;
}

showNewNotification(notifInfo);
