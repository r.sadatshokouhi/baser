

function showData(){
    //static connection...
    var staticSensorsData = [
                        {
                            "id" : 1,
                            "type" : "angular",
                            "kind" : "رطوبت",
                            "name" : "باغچه پایینی",
                            "order" : 3
                        },
                        {
                            "id" : 2,
                            "type" : "cylinder",
                            "kind" : "مخزن آب",
                            "name" : "مخزن آب",
                            "order" : 1
                        },
                        {
                            "id" : 4,
                            "type" : "angular",
                            "kind" : "رطوبت",
                            "name" : "کنار استخر",
                            "order" : 5
                        },
                        {
                            "id" : 3,
                            "type" : "thermometer",
                            "kind" : "دما",
                            "name" : "دماسنج باغ",
                            "order" : 2
                        },
                        {
                            "id" : 5,
                            "type" : "thermometer",
                            "kind" : "دما",
                            "name" : "دماسنج گلخونه",
                            "order" : 7
                        },
                        {
                            "id" : 6,
                            "type" : "angular",
                            "kind" : "رطوبت",
                            "name" : "باغچه جلوی در",
                            "order" : 10
                        },
                    ];
    var staticActorsData = [
                        {
                            "id" : 1,
                            "kind" : "اکتور",
                            "type" : "default",
                            "name" : "مخزن۱",
                            "status" : "on",
                            "order" : 2,
                            "timing": true
                        },
                        {
                            "id" : 2,
                            "kind" : "اکتور",
                            "type" : "default",
                            "name" : "مخزن۲",
                            "status" : "off",
                            "order" : 1,
                            "timing": true
                        },
                        {
                            "id" : 3,
                            "kind" : "اکتور",
                            "type" : "default",
                            "name" : "استخر",
                            "status" : "on",
                            "order" : 4,
                            "timing": true
                        },
                        {
                            "id" : 4,
                            "kind" : "اکتور",
                            "type" : "default",
                            "name" : "باغچه۱",
                            "status" : "off",
                            "order" : 55,
                            "timing": true
                        },
                        {
                            "id" : 5,
                            "kind" : "اکتور",
                            "type" : "default",
                            "name" : "باغچه۲",
                            "status" : "off",
                            "order" : 32,
                            "timing": true
                        }
                    ];    
    //sort data
    staticSensorsData.sort(GetSortOrder("order")); //Pass the attribute to be sorted on 
    staticActorsData.sort(GetSortOrder("order")); 
    //set sorted data
    drawCharts(staticSensorsData);
    drawActors(staticActorsData);
    //dynamic data connection
    var event = setDynamicDataConnection();
    //render charts
    renderCharts(staticSensorsData, event);
    checkNotifications(event);
}

function drawCharts(staticDataJson){
    var charts = '<div class="owl-carousel owl-theme full-width" >'; //we add the carousel here so it show the slide
    // the innerhtml of sensors build here
    for(var i = 0; i < staticDataJson.length; ++i){
        if(staticDataJson[i].type == "angular"){
            charts += drawChartbottomCaption(staticDataJson[i].id, staticDataJson[i].kind, staticDataJson[i].name);
        }
        if(staticDataJson[i].type == "thermometer" || staticDataJson[i].type == "cylinder"){
            charts += drawChartLeftCaption(staticDataJson[i].id, staticDataJson[i].kind, staticDataJson[i].name);
        }
    }
    charts += '</div>';
    // the innerhtml of sensors add here
    document.getElementById("sensor").innerHTML = charts;
}

//Comparer Function    
function GetSortOrder(prop) {    
    return function(a, b) {    
        if (a[prop] > b[prop]) {    
            return 1;    
        } else if (a[prop] < b[prop]) {    
            return -1;    
        }    
        return 0;    
    }    
}    

function setDynamicDataConnection(){
    console.log("start");
    var token = localStorage.getItem('token');
    const evtSource = new EventSource(`http://10.100.100.15:5000/sensors?token=${token}`);
    evtSource.onopen = function() {
    console.log("SSE onopen");
    }
    return evtSource;
}

function renderCharts(staticData, event){
    for(var i = 0; i < staticData.length; i++){
        if(staticData[i].type == "angular"){
            FusionCharts.ready(angularStyle(staticData[i].id, event));
        }
        if(staticData[i].type == "thermometer"){
            FusionCharts.ready(thermometerStyle(staticData[i].id, event));
        }
        if(staticData[i].type == "cylinder"){
            FusionCharts.ready(cylinderStyle(staticData[i].id, event));
        }
    }
}

function drawChartbottomCaption(id, kind, name){
    return '<div class="item">' + 
                '<div id="chart-container-' + id + '" class="text-center">FusionCharts XT will load here!</div>' + 
                '<div class="mt-auto text-center w-100">' + 
                    '<h6 class="card-text mb-4 font-weight-normal">' + 
                    'نوع : ' + kind +
                    '<br/>نام :  '+ name +
                    '</h6>' +
                '</div>' + 
            '</div>';
}

function drawChartLeftCaption(id, kind, name){
    return '<div class="item">' +
                '<div class="row rtl">' + 
                    '<div class="col-lg-8">' +
                        '<div id="chart-container-' + id + '" class="text-center">FusionCharts XT will load here!</div>' +
                    '</div>' +
                    '<div class="col-lg-4">' +
                        '<div class="mt-auto text-center w-100">' + 
                        'نوع : ' + kind +
                        '<br/>نام :  '+ name + 
                        '</div>'+
                    '</div>' +
                '</div>' +
            '</div>';
}

function checkNotifications(evtSource){
    evtSource.addEventListener("message", function(event) {
        var content = JSON.parse(event.data);
        for(var i = 0; i < content.length; i++){
            //if(content[i].) check if its a notif
            icon = "fa-wrench";//default notif icon by now
            setNotifications(content[i], icon);
        }
    });
}


showData();

