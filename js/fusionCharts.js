function cylinderStyle(id, evtSource){
	var chartObj = new FusionCharts({
        type: 'cylinder',
        dataFormat: 'json',
        renderAt: 'chart-container-' + id,
        id: 'chart-id-' + id,
        width: '200',
        height: '280',
        dataSource: {
            "chart": {
                "theme": "fusion",
                "caption": "",
                "subcaption": "",
                "captionFont": "'iransans', arial",
                "captionFontSize": "18",
                "lowerLimit": "0",
                "upperLimit": "120",
                "lowerLimitDisplay": "خالی",
                "upperLimitDisplay": "پر",
                "numberSuffix": "لیتر",
                "showValue": "1",
                "chartBottomMargin": "45",
                "showValue": "0",
                "cylFillColor": "#00b8e6",
                "alignCaptionWithCanvas": "1",
                "captionHorizontalPadding": "0",
                "captionOnTop": "0",
                "captionAlignment": "center",
                "animation" : "0"
            },
            "value": "0",
            "annotations": {
                "origw": "400",
                "origh": "190",
                "autoscale": "1",
                "groups": [{
                    "id": "range",
                    "items": [{
                        "id": "rangeBg",
                        "type": "rectangle",
                        "x": "$canvasCenterX-45",
                        "y": "$chartEndY-30",
                        "tox": "$canvasCenterX +45",
                        "toy": "$chartEndY-75",
                        "fillcolor": "#6caa03"
                    }, {
                        "id": "rangeText",
                        "type": "Text",
                        "fontSize": "11",
                        "fillcolor": "#333333",
                        "text": "0 ltrs",
                        "x": "$chartCenterX-45",
                        "y": "$chartEndY-50"
                    }]
                }]
            }
        }
    });
    chartObj.render();
    evtSource.addEventListener("message", function(event) {
        updateData(event, id, chartObj);
    });
}

function thermometerStyle(id, evtSource){
    var chartObj = new FusionCharts({
        type: 'thermometer',
        renderAt: 'chart-container-' + id,
        id: 'chart-id-' + id,
        width: '240',
        height: '310',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "subcaption": "",
                "captionFont": "'iransans', arial",
                "captionFontSize": "18",
                "lowerLimit": "-10",
                "upperLimit": "0",
                "decimals": "1",
                "numberSuffix": "°C",
                "showhovereffect": "1",
                "thmFillColor": "#008ee4",
                "showGaugeBorder": "1",
                "gaugeBorderColor": "#008ee4",
                "gaugeBorderThickness": "2",
                "gaugeBorderAlpha": "30",
                "thmOriginX": "100",
                "chartBottomMargin": "20",
                "valueFontColor": "#000000",
                "theme": "candy",
                "bgColor" : "#ffffff",
                "showBorder" : "0",
                "animation" : "0",
                "captionOnTop": "0"
            },
            "value": "0",
            //All annotations are grouped under this element
            "annotations": {
                "showbelow": "0",
                "groups": [{
                    //Each group needs a unique ID
                    "id": "indicator",
                    "items": [
                        //Showing Annotation
                        {
                            "id": "background",
                            //Rectangle item
                            "type": "rectangle",
                            "alpha": "50",
                            "fillColor": "#AABBCC",
                            "x": "$gaugeEndX-40",
                            "tox": "$gaugeEndX",
                            "y": "$gaugeEndY+54",
                            "toy": "$gaugeEndY+72"
                        }
                    ]
                }]

            }
            }
    }
    );
    chartObj.render();
    evtSource.addEventListener("message", function(event) {
        updateData(event, id, chartObj);
    });
}
        
function angularStyle(id, evtSource){
    var chartObj = new FusionCharts({
        type: 'angulargauge',
        renderAt: 'chart-container-' + id,
        id: 'chart-id-' + id,
        width: '100%',
        height: '250',
        dataFormat: 'json',
        dataSource: {
            "chart": {
            "caption": "",
            "subcaption": "",
            "captionFont": "'iransans', arial",
            "captionFontSize": "18",
            "lowerLimit": "0",
            "upperLimit": "100",
            "theme": "fusion",
            "animation" : "0",
            "captionOnTop": "0"
        },
        "colorRange": {
            "color": [{
                "minValue": "0",
                "maxValue": "50",
                "code": "#e44a00"
            }, {
                "minValue": "50",
                "maxValue": "75",
                "code": "#f8bd19"
            }, {
                "minValue": "75",
                "maxValue": "100",
                "code": "#6baa01"
            }]
        },
        "dials": {
            "dial": [{
                "value": "0"
            }]
        }
        }
        });
        chartObj.render();
        evtSource.addEventListener("message", function(event) {
            updateData(event, id, chartObj);
        });
}

function updateData(event, id, chartObj){
    var content = JSON.parse(event.data);
    for(var i = 0; i < content.length; i++){
        //check if its for sensors
        if (content[i].id == id){
            var newData = setData(content[i]);
            chartObj.setJSONData(newData);
        }
            
    }
}

function setAngularData(caption, subCaption, newData) {
    return {
        "chart": {
                "caption": 'داده :  ' + caption,
                "subcaption": 'زمان : ' + subCaption,
                "captionFont": "'iransans', arial",
                "captionFontSize": "18",
                "lowerLimit": "0",
                "upperLimit": "100",
                "theme": "fusion",
                "animation" : "0",
                "captionOnTop": "0"
            },
            "colorRange": {
                "color": [{
                    "minValue": "0",
                    "maxValue": "50",
                    "code": "#e44a00"
                }, {
                    "minValue": "50",
                    "maxValue": "75",
                    "code": "#f8bd19"
                }, {
                    "minValue": "75",
                    "maxValue": "100",
                    "code": "#6baa01"
                }]
            },
            "dials": {
                "dial": [{
                    "value": newData
                }]
            }
        }
};

function setCylinderData(caption, subCaption, newData) {
    return {
        "chart": {
            "theme": "fusion",
            "caption": caption,
            "subcaption": subCaption,
            "captionFont": "'iransans', arial",
            "captionFontSize": "18",
            "lowerLimit": "0",
            "upperLimit": "120",
            "lowerLimitDisplay": "خالی",
            "upperLimitDisplay": "پر",
            "numberSuffix": "لیتر",
            "showValue": "1",
            "chartBottomMargin": "45",
            "showValue": "0",
            "cylFillColor": "#00b8e6",
            "alignCaptionWithCanvas": "1",
            "captionHorizontalPadding": "0",
            "captionOnTop": "0",
            "captionAlignment": "center",
            "animation" : "0"
        },
        "value": newData,
        "annotations": {
            "origw": "400",
            "origh": "190",
            "autoscale": "1",
            "groups": [{
                "id": "range",
                "items": [{
                    "id": "rangeBg",
                    "type": "rectangle",
                    "x": "$canvasCenterX-45",
                    "y": "$chartEndY-30",
                    "tox": "$canvasCenterX +45",
                    "toy": "$chartEndY-75",
                    "fillcolor": "#6caa03"
                }, {
                    "id": "rangeText",
                    "type": "Text",
                    "fontSize": "11",
                    "fillcolor": "#333333",
                    "text": String(newData) + "ltrs",
                    "x": "$chartCenterX-45",
                    "y": "$chartEndY-50"
                }]
            }]
        }
    };
}

function setThermometerData(caption, subCaption, newData) {
    return {
        "chart": {
            "caption": caption,
            "subcaption": subCaption,
            "captionFont": "'iransans', arial",
            "captionFontSize": "18",
            "lowerLimit": "-10",
            "upperLimit": "0",
            "decimals": "1",
            "numberSuffix": "°C",
            "showhovereffect": "1",
            "thmFillColor": "#008ee4",
            "showGaugeBorder": "1",
            "gaugeBorderColor": "#008ee4",
            "gaugeBorderThickness": "2",
            "gaugeBorderAlpha": "30",
            "thmOriginX": "100",
            "chartBottomMargin": "20",
            "valueFontColor": "#000000",
            "theme": "candy",
            "bgColor" : "#ffffff",
            "showBorder" : "0",
            "animation" : "0",
            "captionOnTop": "0"
        },
        "value": newData,
        //All annotations are grouped under this element
        "annotations": {
            "showbelow": "0",
            "groups": [{
                //Each group needs a unique ID
                "id": "indicator",
                "items": [
                    //Showing Annotation
                    {
                        "id": "background",
                        //Rectangle item
                        "type": "rectangle",
                        "alpha": "50",
                        "fillColor": "#AABBCC",
                        "x": "$gaugeEndX-40",
                        "tox": "$gaugeEndX",
                        "y": "$gaugeEndY+54",
                        "toy": "$gaugeEndY+72"
                    }
                ]
            }]
    
        }
        };
}

function setData(dataObj){
    var date = new persianDate(dataObj.timestamp).format();
    if (dataObj.type == "angular")
        return setAngularData(String(dataObj.data), date, dataObj.data);
    if (dataObj.type == "cylinder")
        return setCylinderData(String(dataObj.data), date, dataObj.data);
    if(dataObj.type == "thermometer")
        return setThermometerData(String(dataObj.data), date, dataObj.data);
}
