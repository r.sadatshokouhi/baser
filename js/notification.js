function setNotifications(notifObj, icon){
    //increase notif count
    var countNotif = document.getElementById("count-notif").innerHTML;
    document.getElementById("count-notif").innerHTML = parseInt(countNotif) + 1;
    var countNotifMsg = "شما " + (parseInt(countNotif) + 1) + " پیام جدید دارید ." ;
    document.getElementById("notif-drop-message").innerHTML = countNotifMsg;

    var notifDropDown = document.getElementById("notif-drop");

    //create divider line
    var notifDivider = document.createElement("div");
    notifDivider.classList.add("dropdown-divider");
    notifDropDown.appendChild(notifDivider);

    //create a part for notif in drop down list
    var notifPartial = document.createElement("a");
    notifPartial.classList.add("dropdown-item");
    notifPartial.classList.add("preview-item");

    //create notif icon
    var notifMessage = document.createElement("div");
    notifMessage.classList.add("preview-thumbnail");
    var notifIconWrapper = document.createElement("div");
    notifIconWrapper.classList.add("preview-icon");
    notifIconWrapper.classList.add("bg-danger");
    var notifMessageIcon = document.createElement("i");
    notifMessageIcon.classList.add("fas");
    notifMessageIcon.classList.add(icon);
    notifMessageIcon.classList.add("mx-0");
    notifIconWrapper.appendChild(notifMessageIcon);
    notifMessage.appendChild(notifIconWrapper);
    notifPartial.appendChild(notifMessage);

    //create notif message
    var notifContentWrapper = document.createElement("div");
    notifContentWrapper.classList.add("preview-item-content");
    var notifContent = document.createElement("h6");
    notifContent.classList.add("preview-subject");
    notifContent.classList.add("font-weight-medium");
    notifContent.innerHTML = notifObj.message;
    notifContentWrapper.appendChild(notifContent);
    notifPartial.appendChild(notifContentWrapper);

    //add whole made part to notifications
    notifDropDown.appendChild(notifPartial);
}


var countNotif = document.getElementById("count-notif").innerHTML;
document.getElementById("count-notif").innerHTML = parseInt(countNotif) + 1;
var countNotifMsg = "شما " + (parseInt(countNotif) + 1) + " پیام جدید دارید ." ;
document.getElementById("notif-drop-message").innerHTML = countNotifMsg;

var notifDropDown = document.getElementById("notif-drop");

var notifDivider = document.createElement("div");
notifDivider.classList.add("dropdown-divider");
notifDropDown.appendChild(notifDivider);

var notifPartial = document.createElement("a");
notifPartial.classList.add("dropdown-item");
notifPartial.classList.add("preview-item");

var notifMessage = document.createElement("div");
notifMessage.classList.add("preview-thumbnail");
var notifIconWrapper = document.createElement("div");
notifIconWrapper.classList.add("preview-icon");
notifIconWrapper.classList.add("bg-danger");
var notifMessageIcon = document.createElement("i");
notifMessageIcon.classList.add("fas");
notifMessageIcon.classList.add("fa-exclamation-circle");
notifMessageIcon.classList.add("mx-0");
notifIconWrapper.appendChild(notifMessageIcon);
notifMessage.appendChild(notifIconWrapper);
notifPartial.appendChild(notifMessage);

var notifContentWrapper = document.createElement("div");
notifContentWrapper.classList.add("preview-item-content");
var notifContent = document.createElement("h6");
notifContent.classList.add("preview-subject");
notifContent.classList.add("font-weight-medium");
notifContent.innerHTML = "پیام جدییید XD";
notifContentWrapper.appendChild(notifContent);
notifPartial.appendChild(notifContentWrapper);

notifDropDown.appendChild(notifPartial);

document.getElementById("showAllNotifs").addEventListener("click", function(){
    window.location.href = "notifications.html";
});
